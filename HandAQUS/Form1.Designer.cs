﻿namespace HandAQUS
{
    partial class HandAQUS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
       
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HandAQUS));
            this.scribblePanel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addFolderButton = new System.Windows.Forms.ToolStripMenuItem();
            this.autoSaveButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveButton = new System.Windows.Forms.ToolStripMenuItem();
            this.loadButton = new System.Windows.Forms.ToolStripMenuItem();
            this.clearButton = new System.Windows.Forms.ToolStripMenuItem();
            this.exitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.switchModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.stateLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scribblePanel
            // 
            this.scribblePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scribblePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.scribblePanel.BackColor = System.Drawing.SystemColors.Window;
            this.scribblePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scribblePanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.scribblePanel.Location = new System.Drawing.Point(108, 30);
            this.scribblePanel.Margin = new System.Windows.Forms.Padding(5);
            this.scribblePanel.Name = "scribblePanel";
            this.scribblePanel.Padding = new System.Windows.Forms.Padding(5);
            this.scribblePanel.Size = new System.Drawing.Size(1084, 630);
            this.scribblePanel.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFolderButton,
            this.autoSaveButton,
            this.saveButton,
            this.loadButton,
            this.clearButton,
            this.exitButton,
            this.switchModeButton});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(98, 674);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addFolderButton
            // 
            this.addFolderButton.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold);
            this.addFolderButton.Image = ((System.Drawing.Image)(resources.GetObject("addFolderButton.Image")));
            this.addFolderButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.addFolderButton.Name = "addFolderButton";
            this.addFolderButton.Size = new System.Drawing.Size(91, 96);
            this.addFolderButton.Text = "Folder";
            this.addFolderButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addFolderButton.Click += new System.EventHandler(this.addFolderButton_Click);
            // 
            // autoSaveButton
            // 
            this.autoSaveButton.Enabled = false;
            this.autoSaveButton.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.autoSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("autoSaveButton.Image")));
            this.autoSaveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.autoSaveButton.Name = "autoSaveButton";
            this.autoSaveButton.Size = new System.Drawing.Size(91, 91);
            this.autoSaveButton.Text = "AutoSave";
            this.autoSaveButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.autoSaveButton.Click += new System.EventHandler(this.autoSaveButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold);
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 96);
            this.saveButton.Text = "Save As..";
            this.saveButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold);
            this.loadButton.Image = ((System.Drawing.Image)(resources.GetObject("loadButton.Image")));
            this.loadButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(91, 96);
            this.loadButton.Text = "Load";
            this.loadButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold);
            this.clearButton.Image = ((System.Drawing.Image)(resources.GetObject("clearButton.Image")));
            this.clearButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 96);
            this.clearButton.Text = "Clear";
            this.clearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold);
            this.exitButton.Image = ((System.Drawing.Image)(resources.GetObject("exitButton.Image")));
            this.exitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 96);
            this.exitButton.Text = "Exit";
            this.exitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // switchModeButton
            // 
            this.switchModeButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.switchModeButton.Enabled = false;
            this.switchModeButton.Font = new System.Drawing.Font("Sitka Text", 10F);
            this.switchModeButton.Image = ((System.Drawing.Image)(resources.GetObject("switchModeButton.Image")));
            this.switchModeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.switchModeButton.Name = "switchModeButton";
            this.switchModeButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.switchModeButton.Size = new System.Drawing.Size(91, 88);
            this.switchModeButton.Text = "Switch mode";
            this.switchModeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.switchModeButton.Visible = false;
            this.switchModeButton.Click += new System.EventHandler(this.switchModeButton_Click);
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.stateLabel.ForeColor = System.Drawing.Color.Black;
            this.stateLabel.Location = new System.Drawing.Point(106, 9);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(233, 15);
            this.stateLabel.TabIndex = 2;
            this.stateLabel.Text = "Folder for autosave was not selcted";
            // 
            // HandAQUS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ClientSize = new System.Drawing.Size(1206, 674);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.scribblePanel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HandAQUS";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HandAQUS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel scribblePanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveButton;
        private System.Windows.Forms.ToolStripMenuItem loadButton;
        private System.Windows.Forms.ToolStripMenuItem clearButton;
        private System.Windows.Forms.ToolStripMenuItem exitButton;
        private System.Windows.Forms.ToolStripMenuItem addFolderButton;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.ToolStripMenuItem autoSaveButton;
        private System.Windows.Forms.ToolStripMenuItem switchModeButton;
    }
}

