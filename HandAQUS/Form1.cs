﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WintabDN;

namespace HandAQUS
{
    public partial class HandAQUS : Form 
    {   
        // Define variables for WinTab API
        private CWintabContext m_logContext = null;
        private CWintabData m_wtData = null;

        private Graphics m_graphics;
        private Pen m_pen;
        private Pen m_backPen;

        private Int32 m_pkX = 0;
        private Int32 m_pkY = 0;
        private UInt32 m_pressure = 0;
        private UInt64 m_pkTime = 0;
        private UInt64 m_pkTimeLast = 0;

        private Point m_lastPoint = Point.Empty;
        private UInt32 m_maxPkts = 1; // max num pkts to capture/display at a time

        // Keep captured data as packet List for saving
        private List<WintabPacket> sessionData = new List<WintabPacket>();
        
        // This keep loaded data from *svc file
        private List<Int64[]> openFileList = new List<Int64[]>();

        // Variables for autoSave
        private string saveFolderPath = null;
        private string saveFolderName = null;
        private string saveFileName = null;
        private uint saveFileCounter = 0;

        // This is for get maximal Axes values, to set Gird
        // Default vale for read only mode are Intos5 L maximal values 
        private Int32 m_TABEXTX = 65023;
        private Int32 m_TABEXTY = 40639;
        private Int16 m_PRESSURE_MAX = 200;

        // This constant serve to divide maximal pressure of device  
        // to obtain m_PRESSURE_MAX
        private Byte bulgar_constant = 5;

        // Edit Mode flag, false by default
        private bool b_readOnlyMode = false;
        
        public HandAQUS()
        {          
           InitializeComponent();
           Shown += new EventHandler(OpenScrible);            
        }
        private void OpenScrible(object sender, EventArgs e)
        {
            // If read only mode is selected shows environment only for reading
            if (b_readOnlyMode)
            {
                saveButton.Visible = false;
                autoSaveButton.Visible = false;
                addFolderButton.Visible = false;
                switchModeButton.Visible = true;
                switchModeButton.Enabled = true;
                stateLabel.Text = "Read-only mode is active";
                stateLabel.ForeColor = Color.DarkGreen;                
            }
            // If Tablet is connected Acquisition is allowed
            else
            {
                stopScribble();
                ClearDisplay();
                startScribble();
            }
        }


        private void addFolderButton_Click(object sender, EventArgs e)
        {
            // Prepare folder browser dialog
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            
            // If previously, folder was open then put it as actual folder
            if (saveFolderPath != null)
            {
                folderBrowser.SelectedPath = saveFolderPath;
            }

            // Now map folder for autosave
            if (folderBrowser.ShowDialog() == DialogResult.OK) 
            {
                saveFileCounter = 1;
                // Set Folder path
                saveFolderPath = folderBrowser.SelectedPath;
                // Set Folder name
                saveFolderName = saveFolderPath.Split('\\').Last();
                // Set File name
                saveFileName = string.Format("{0}_{1}.svc", saveFolderName, saveFileCounter.ToString().PadLeft(4,'0'));
                // Show name of file
                stateLabel.Text = saveFileName; 
                autoSaveButton.Enabled = true;
            }
        }    

        private void loadButton_Click(object sender, EventArgs e)
        {
            // If there are some unsaved captured data, ask to continue
            if (sessionData.Any()) 
            {
                DialogResult dialogResult = MessageBox.Show("All captured data will be lost.\nDo you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                // If yes delete data and continue with loading
                if (dialogResult == DialogResult.Yes)
                {
                    sessionData.Clear();
                    ClearDisplay();                    
                    m_graphics = null;
                }
                else
                {
                    return;
                }
            }
            // There are no data captured so *.svc file can be loaded.
            // Initialize File Dialog for file selection
            Stream myStream = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // Filter .svc files only
            openFileDialog.Filter = "svc files (*.svc)|*.svc|All files (*.*)|*.*"; 
            openFileDialog.FilterIndex = 1;
            // Restore from last directory
            openFileDialog.RestoreDirectory = true;
            
            // Initial clear of list variable 
            openFileList.Clear();

            // Choose file
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // Open stream for file processing
                    if ((myStream = openFileDialog.OpenFile()) != null)
                    {
                        // Ensure that current stream will be used
                        using (myStream) 
                        {
                            // Clear panel
                            scribblePanel.Invalidate(); 
                            
                            // Read file line by line and store it to var
                            var lines = File.ReadAllLines(openFileDialog.FileName);
                            
                            // Convert lines (strings) to numbers and store them into List
                            foreach (var line in lines)
                            {
                                // Trim spaces on begin and end of string
                                string t_line = line.Trim();
                                // Split by space (' ') and convert to number
                                Int64[] numbers = Array.ConvertAll(t_line.Split(' '), Int64.Parse);
                                // Add values to List
                                openFileList.Add(numbers);
                            }
                            // Now draw data
                            drawDataFromFile(openFileList);
                            // Disable possibility to save again loaded data
                            saveButton.Enabled = false; 
                            autoSaveButton.Enabled = false;
                        }
                        // Clear data
                        openFileList.Clear();
                        // Close stream
                        myStream.Close();
                    }
                    // Disable option to capture data from tablet
                    m_graphics = null;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk (line: " + openFileList.Count + "). Original error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            // This cause that no other data will be captured
            // Also cursor control by pen stay disabled
            m_graphics = null;

            // First check if there is some data, than continue
            if (sessionData.Any()) 
            {
                // Initialize File Dialog for save file
                Stream myStream;
                SaveFileDialog saveFile = new SaveFileDialog();
                // Force to save in .svc format
                saveFile.Filter = "svc files (*.svc)|*.svc|All files (*.*)|*.*";
                saveFile.FilterIndex = 1;
                // Restore last directory
                saveFile.RestoreDirectory = true;
                saveFile.FileName = saveFileName;

                // Save file
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    // Prepare stream
                    if ((myStream = saveFile.OpenFile()) != null)
                    {
                        // Create stream writer and ensure that this stream writer is used
                        using (StreamWriter sw = new StreamWriter(myStream))
                        {
                            // Store data to file
                            if (StoreData(sw))
                            {
                                // Clear session data
                                sessionData.Clear();
                                // Close file stream
                                myStream.Close();
                            }
                        }
                    }
                    // Allow scribble after file is saved and panel is cleared
                    startScribble();
                }
                else
                {
                    // Continue in scribble
                    m_graphics = scribblePanel.CreateGraphics();
                    m_graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    return;
                }                
            }
            else // No Data to save
            {
                MessageBox.Show("No data to save!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                startScribble();
            }
        }

        private void autoSaveButton_Click(object sender, EventArgs e)
        {
            // This cause that no other data will be captured
            // Also cursor control by pen stay disabled
            m_graphics = null;

            // Check if there are any capture data
            if (sessionData.Any())
            {
                string path = Path.Combine(saveFolderPath, string.Format("{0}_{1}.svc", saveFolderName, saveFileCounter.ToString().PadLeft(4, '0')));
                if (File.Exists(path))
                {
                    DialogResult dialogResult = MessageBox.Show("File already exist. Do you want to rewrite file?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dialogResult == DialogResult.No)
                    {
                        // Continue in scribble
                        m_graphics = scribblePanel.CreateGraphics();
                        m_graphics.SmoothingMode = SmoothingMode.AntiAlias;
                        return;
                    }
                }
                // Create stream writer and ensure that this stream writer is used
                using (StreamWriter sw = new StreamWriter(path))
                {
                    // Store data to file
                    if (StoreData(sw))
                    {
                        // Clear session data
                        sessionData.Clear();
                        // Close file stream                        
                        path = null;
                    }                    
                }
                // Allow scribble after file is saved and panel is cleared                
                startScribble();               
            }
            else // No Data to save
            {
                MessageBox.Show("No data to save!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                startScribble();
            }
        }         

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to exit HandAQUS?", "Important Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes) {
                Application.Exit();
            }           
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            // Ask first for sure.
            DialogResult dialogResult = MessageBox.Show("Do you want to clear panel?", "Important Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            // If yes clear panel and remove data
            if (dialogResult == DialogResult.Yes)
            {
                // Stop data capturing
                stopScribble();
                // Clear display
                ClearDisplay();

                // If read only mode is activated do not allow to start Scribble
                if (!b_readOnlyMode)
                {
                    // After Load button, this options are disabled so enable it
                    saveButton.Enabled = true;
                    if (saveFolderName != null) { autoSaveButton.Enabled = true; }
                    // Start data capturing
                    startScribble();
                }
            }
           
        }

        private void switchModeButton_Click(object sender, EventArgs e)
        {
            // Ask for mode change to Acquisition
            DialogResult dialogResult = MessageBox.Show("Activate acquisition mode?\n", "Important Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    // If yes look if Wintab is available
                    if (testWinatbAvailiability())
                    {
                        // If yes, open Acquisition mode
                        b_readOnlyMode = false;
                        this.Shown += new System.EventHandler(this.OpenScrible);
                        DefaultEnviroment();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Can't access Wintab driver: " + ex.ToString());
                }
            }
        }

        private void ClearDisplay()
        {
            // Clear Panel
            scribblePanel.Invalidate();
            // Clear sessionData (all packets)
            sessionData = null;
            sessionData = new List<WintabPacket>();
        }

        private void CloseCurrentContext()
        {
            try
            {
                // First check if context is not null yet
                if (m_logContext != null)
                {
                    // If is not null close it
                    if (!m_logContext.Close())
                    {
                        throw new Exception("Can not close the context!");                            
                    }                        

                    // Now set context and captured data to null
                    m_logContext = null;
                    m_wtData = null;
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
        }

        private void startScribble()
        {   
            // Close context and clear panel
            CloseCurrentContext();
            ClearDisplay();          
          
            // Set up to capture 1 packet at a time.
            m_maxPkts = 1;

            // Init scribble graphics.
            m_graphics = scribblePanel.CreateGraphics();
            m_graphics.SmoothingMode = SmoothingMode.AntiAlias;

            // Set pen color
            m_pen = new Pen(Color.Black);
            m_backPen = new Pen(Color.White);
       
            // Set cursor control to false
            bool controlSystemCursor = false;
            // Initialize data capture. Now you should be able to scribble
            InitDataCapture(controlSystemCursor);             
        }

        private void stopScribble()
        {
            // Close scribble context.
            CloseCurrentContext();

            // Turn off graphics.
            if (m_graphics != null)
            {
                m_graphics = null;
            }
        }


        private void InitDataCapture(bool ctrlSysCursor_I = false)
        {
            try
            {
                // Close context from any previous run.
                CloseCurrentContext();

                // Open up a new context with cursor control flag (false by default)
                m_logContext = OpenTestDigitizerContext(ctrlSysCursor_I);

                // If context was not open, return
                if (m_logContext == null)
                {
                    return;
                }

                // Create a data object and set its WT_PACKET handler. 
                m_wtData = new CWintabData(m_logContext);
                m_wtData.SetWTPacketEventHandler(MyWTPacketEventHandler);
                m_wtData.SetPacketQueueSize(150);
                                
                //stateLabel.Text=string.Format("{0}",m_logContext.MoveMask);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private CWintabContext OpenTestDigitizerContext(bool ctrlSysCursor = true)
        {
            bool status = false;
            // Init wintab context variable
            CWintabContext logContext = null;            

            try
            {
                // Get the default digitizing context.
                // Default is to receive data events.
                logContext = CWintabInfo.GetDefaultDigitizingContext(ECTXOptionValues.CXO_MESSAGES);
                logContext.Options |= (uint)ECTXOptionValues.CXO_MGNINSIDE;
           
                // Set system cursor if caller wants it (if true).
                if (ctrlSysCursor)
                {
                    logContext.Options |= (uint)ECTXOptionValues.CXO_SYSTEM;                    
                }

                // If context was not open, return null
                if (logContext == null)
                {
                    return null;
                }

                // Modify the digitizing region.
                // Set name
                logContext.Name = "WintabDN Event Data Context";
                // Set packet mode to get button status in 0/1.
                logContext.PktMode = 0;
                // Set X,Y origin of the context's input area in the tablet's native coordinates
                logContext.InOrgX = logContext.InOrgY = 0;

                // Open the context, which will also tell Wintab to send data packets.
                status = logContext.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("OpenTestDigitizerContext ERROR: " + ex.ToString());
            }

            return logContext;
        }


        ///////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Called when Wintab WT_PACKET events are received.
        /// </summary>
        /// <param name="sender_I">The EventMessage object sending the report.</param>
        /// <param name="eventArgs_I">eventArgs_I.Message.WParam contains ID of packet containing the data.</param>   
        public void MyWTPacketEventHandler(Object sender_I, MessageReceivedEventArgs eventArgs_I)
        {
            try
            {
                // Maximum of processed packet at once have to be 1
                if (m_maxPkts == 1)
                {
                    // Get packet ID
                    uint pktID = (uint)eventArgs_I.Message.WParam;
                    // Get packet from data by packet ID
                    WintabPacket pkt = m_wtData.GetDataPacket(pktID);                       

                    // If packet is not empty
                    if (pkt.pkContext != 0)
                    {
                        // Read axis and pressure
                        m_pkX = pkt.pkX; 
                        m_pkY = pkt.pkY;
                        m_pressure = pkt.pkNormalPressure;

                        // Read time-stamp
                        m_pkTime = pkt.pkTime;

                        // If Graphics is not null process packet
                        if (m_graphics != null)
                        {
                            // Add packet to session data
                            sessionData.Add(pkt);

                            // Scribble mode
                            // get info of panel resolution
                            int clientWidth = scribblePanel.Width;
                            int clientHeight = scribblePanel.Height;
                            // Draw a point

                            //stateLabel.Text = string.Format("m_pkX {0} m_pkY {1} ; cW{2} cH{3}; tX{4}", m_pkX,m_pkY,clientWidth,clientHeight,m_TABEXTX);
                            DrawPoint(clientWidth, clientHeight);                                                      
                        }
                    }
                    // Flush current data packets
                    pkt.pkSerialNumber = 0;
                    m_wtData.FlushDataPackets(pktID);

                }
            }
            catch (Exception ex)
            {
               throw new Exception("FAILED to get packet data: " + ex.ToString());                
            }
        }       

        // Remove first captured in-air data 
        private void RemoveFirstInAirData(List<WintabPacket> listOfPacket)
        {
            int CountToFisrtOne = 0;
            for (int i = 0; i < listOfPacket.Count; i++ )
            {
                if (listOfPacket[i].pkButtons != 0)
                {
                    CountToFisrtOne=i;
                    break;
                }                
            }
            listOfPacket.RemoveRange(0,CountToFisrtOne);    
        }

        // Remove last captured in-air data
        private void RemoveLastInAirData(List<WintabPacket> listOfPacket)
        {
            int firstlastZeroPacketIndex = 0;
            int CountToTheEnd = 0;

            for (int i = listOfPacket.Count - 1; i > 0; i--)
            {
                if (listOfPacket[i].pkButtons != 0)
                {
                    firstlastZeroPacketIndex = i;
                    CountToTheEnd = listOfPacket.Count - i;
                    break;
                }                
            }
            listOfPacket.RemoveRange(firstlastZeroPacketIndex, CountToTheEnd);      
        }

        private void ShowSavedFile(string filename)
        {
            AutoClosingMessageBox.Show(String.Format("File {0} was saved successfully", filename),"Info",1000);
        }

        // Test if Wintab driver is available. If Yes look if device is connected and if also yes open Acquisition mode.
        // If Wintab is not available or device is not connected ask for read-only mode
        public bool TestWintabAvailible()         
        {
            // Check if driver or device is connected
            if (!CWintabInfo.IsWintabAvailable() || CWintabInfo.GetNumberOfDevices() == 0)
            {
                // If no, Ask for read-only mode
                DialogResult dialogResult = MessageBox.Show("Wacom Tablet is not connected!\n" +
                                            "Do you want to continue in \"read only\" mode?.\n",
                                            "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    // Activate read-only mode
                    b_readOnlyMode = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            // If driver is available, check for device
            else if (CWintabInfo.GetNumberOfDevices() != 0)
            {
                // If device is connected, return true and set Axis maximal value to set Gird
                setAxisAndPressureMax();
                return true;                
            }
            else
            { 
                MessageBox.Show("Wintab was not found!\nCheck if tablet driver service is running.\n", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }            
        }        

        private void drawDataFromFile(List<Int64[]> data)
        {
            try
            {
                // Init scribble graphics.
                // m_graphics = CreateGraphics();
                m_graphics = scribblePanel.CreateGraphics();
                m_graphics.SmoothingMode = SmoothingMode.AntiAlias;

                // Set values for Pen colors and init points variables
                m_pen = new Pen(Color.Black);
                m_backPen = new Pen(Color.White);
                // Set last point as Empty
                m_lastPoint = Point.Empty;

                // If Graphics is not null process drawing
                if (m_graphics != null)
                {
                    // Scribble mode
                    // get info of panel resolution
                    int clientWidth = scribblePanel.Width;
                    int clientHeight = scribblePanel.Height;

                    // For each point in loaded data 
                    foreach (Int64[] packet in data)
                    {
                        // If packet do not contains all information or pen is in Air-mode then skip
                        if (packet.Length < 7 || packet[3] == 0)
                        {
                            // Set last point as empty
                            m_lastPoint = Point.Empty;
                            continue;
                        }
                        else
                        {
                            // Read axis, time-stamp and pressure
                            m_pkX = (UInt16)packet[0]; 
                            m_pkY = (UInt16)packet[1]; 
                            m_pkTime = (UInt64)packet[2];                            
                            m_pressure = (UInt32)packet[6];

                            // Draw a point 
                            DrawPoint(clientWidth,clientHeight);
                        }
                    }
                }

            }
             catch (Exception ex)
            {
                throw new Exception("FAILED to get data: " + ex.ToString());
            }
        }        
        
        // Test if Wintab is available for switch mode purpose
        private bool testWinatbAvailiability()
        {
            if (!CWintabInfo.IsWintabAvailable())
            {
                MessageBox.Show("Wintab driver was not found!\nCheck if tablet driver service is running.\n", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (CWintabInfo.GetNumberOfDevices() == 0)
            {
                MessageBox.Show("Tablet is not connected!.\n", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                setAxisAndPressureMax();
                return true;
            }

        }
        
        private void DefaultEnviroment()
        {
            // Set default environment for data Acquisition mode
            saveButton.Visible = true;
            saveButton.Enabled = true;
            autoSaveButton.Visible = true;
            autoSaveButton.Enabled = false;
            addFolderButton.Visible = true;
            switchModeButton.Visible = false;
            switchModeButton.Enabled = false;
            stateLabel.Text = "Folder for autosave was not selected";
            stateLabel.ForeColor = System.Drawing.Color.Black;

            this.startScribble();
        }

        private void setAxisAndPressureMax()
        {
            // set axis maximums and pressure maximum 
            m_TABEXTX = CWintabInfo.GetTabletAxis(EAxisDimension.AXIS_X).axMax;
            m_TABEXTY = CWintabInfo.GetTabletAxis(EAxisDimension.AXIS_Y).axMax;
            m_PRESSURE_MAX = (short)(CWintabInfo.GetMaxPressure(true) / bulgar_constant);
        }
        
        private bool StoreData(StreamWriter sw)
        {
            try
            {
                // read values from sessionData packet by packet!!
                // REMOVE FIRST AND LAST IN AIR DATA
                RemoveFirstInAirData(sessionData);
                RemoveLastInAirData(sessionData);

                // First line is number of samples (one number)
                sw.Write(string.Format("{0}\n", sessionData.Count));

                // Write packets into file line by line in following order:
                // X   Y   TimeStamp   ButtonStatus    Azimuth     Altitude    Pressure         
                foreach (WintabPacket packet in sessionData)
                {
                    sw.Write(string.Format("{0} {1} {2} {3} {4} {5} {6}\n", packet.pkX, packet.pkY, packet.pkTime, packet.pkButtons,
                            packet.pkOrientation.orAzimuth, packet.pkOrientation.orAltitude, packet.pkNormalPressure), Text);
                }
                // Close stream writer
                sw.Flush();
                sw.Close();
                // Show info box with saved file for 2 second
                ShowSavedFile(saveFileName);

                // If folder for autosave is mapped, increment file name
                if (saveFolderName != null)
                {
                    saveFileCounter++;
                    // Set File name
                    saveFileName = string.Format("{0}_{1}.svc", saveFolderName, saveFileCounter.ToString().PadLeft(4, '0'));
                    // Show file name on status label
                    stateLabel.Text = saveFileName;
                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Not possible to write a stream! Original exception: " + ex.Message,"Warning", MessageBoxButtons.OK ,MessageBoxIcon.Exclamation);
                return false;
            }            
        }
        
        private void DrawPoint(int clientWidth, int clientHeight)
        {
            //ScaleXY();
            // set X and Y points according to panel resolution and tablet maximum ranges
            int X = (int)((m_pkX * clientWidth *2) / (double)m_TABEXTX);
            int Y = (int)(clientHeight - ((m_pkY * clientHeight) / (double)m_TABEXTY));

            // create point for drawing on panel
            Point tabPoint = new Point(X, Y);

            // update scribble panel
            scribblePanel.Update();

            // if last point which was drawn is empty (e.g. first point)
            if (m_lastPoint.Equals(Point.Empty))
            {
                // set new point as a last one, included time-stamp
                m_lastPoint = tabPoint;
                m_pkTimeLast = m_pkTime;
            }            
            // set pen width,
            m_pen.Width = (float)(m_pressure / (double)m_PRESSURE_MAX);

            // if pressure is bigger than 0 (on-surface movement) draw a point (line between points)
            if (m_pressure > 0)
            {
                // If time diff between current and last point is 
                // lower than 5 connect this points by rectangle 
                if (m_pkTime - m_pkTimeLast < 5)
                {
                    // This state is according to wintab demo. I don't know the reason.
                    // But probably this state never occur
                    m_graphics.DrawRectangle(m_pen, X, Y, 1, 1);
                }
                else
                {
                    // Draw a line between points
                    m_graphics.DrawLine(m_pen, tabPoint, m_lastPoint);
                }
            }

            // Set current point as a last one
            m_lastPoint = tabPoint;
            m_pkTimeLast = m_pkTime;

        }

        private void ScaleXY()
        {

            if (Math.Sign(m_logContext.OutExtX) == Math.Sign(m_logContext.InExtX))
            {
                m_pkX = (((m_pkX - m_logContext.InOrgX) * Math.Abs(m_logContext.OutExtX)) / Math.Abs(m_logContext.InExtX)) + m_logContext.OutOrgX;
            } else
            {
                m_pkX = ((Math.Abs(m_logContext.InExtX) - (m_pkX - m_logContext.InOrgX)) * Math.Abs(m_logContext.OutExtX)) / Math.Abs(m_logContext.InExtX) + m_logContext.OutOrgX;
            }

            if (Math.Sign(m_logContext.OutExtY) == Math.Sign(m_logContext.InExtY))
            {
                m_pkY = (((m_pkY - m_logContext.InOrgY) * Math.Abs(m_logContext.OutExtY)) / Math.Abs(m_logContext.InExtY)) + m_logContext.OutOrgY;
            }
            else
            {
                m_pkY = ((Math.Abs(m_logContext.InExtY) - (m_pkY - m_logContext.InOrgY)) * Math.Abs(m_logContext.OutExtY)) / Math.Abs(m_logContext.InExtY) + m_logContext.OutOrgY;
            }


        }
    }
}
