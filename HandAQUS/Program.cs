﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace HandAQUS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);            
            HandAQUS app = new HandAQUS();
            if (app.TestWintabAvailible())
            {
                try
                {
                    Application.Run(app);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: The HandAQUS can not start. Original error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else 
            {
                Application.Exit();
            }
        }
    }
}
